user_won = 0
user_lost = 0
total_times = 1_000_000

total_times.times do
  doors = [1, 2, 3]
  zonk_at = doors.sample
  user_picks_door = doors.sample
  host_opens_door = (doors - [zonk_at, user_picks_door]).sample
  change_door = (doors - [user_picks_door, host_opens_door]).first


  if change_door == zonk_at
    user_won += 1
  else
    user_lost += 1
  end
end

puts "If the user always changes door, in #{total_times} runs:"
puts "Wins: #{user_won}, Loses: #{user_lost}"